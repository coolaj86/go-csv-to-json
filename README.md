# go-csv-to-json

Read in a full-spec CSV (y'know, the nasty kind), get back JSON.

- [x] escaped quotes
- [x] newlines
- [x] a variable number of columns

(all standard Go csv parser stuff)

# Usage

```bash
git clone https://git.coolaj86.com/coolaj86/go-csv-to-json.git
pushd go-csv-to-json/
```

```bash
go run csv.go /path/to/import.csv /path/to/export.json
```

# TODO

Web API
