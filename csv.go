package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	if 3 != len(os.Args) {
		fmt.Println("Usage: go run csv.go file.csv")
		os.Exit(1)
	}
	rpath := os.Args[1]
	wpath := os.Args[2]

	f, err := os.Open(rpath)
	if nil != err {
		log.Fatal("open csv file for reading:", err)
	}

	fw, err := os.OpenFile(wpath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0750)
	if nil != err {
		log.Fatal("open json file for writing:", err)
	}

	b := []byte("[\n")
	r := csv.NewReader(f)
	for {
		row, err := r.Read()
		if nil != err {
			if io.EOF == err {
				_, err = fw.Write([]byte("\n]\n"))
				break
			}
			// deceptive: the one error embeds another error
			if err, ok := err.(*csv.ParseError); !ok || err.Err != csv.ErrFieldCount {
				log.Fatal("error while parsing csv:", err)
			}
		}
		fmt.Println(len(row))

		// write previous byte ("[", or ","{
		_, err = fw.Write(b)
		if nil != err {
			log.Fatal("file write", err)
		}
		// string array to json
		b, err = json.Marshal(row)
		if nil != err {
			log.Fatal("marshal row to json", err)
		}
		// write string array
		_, err = fw.Write(b)
		if nil != err {
			log.Fatal("file write", err)
		}
		// assign next byte
		b = []byte(",\n")
	}

	err = fw.Close()
	if nil != err {
		log.Fatal("close json file:", err)
	}
}
